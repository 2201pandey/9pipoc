MeteorPackage = React.createClass({
	render(){
		var steps = [{
			_id: 1,
			className: 'list-group-item',
			text: 'Run the method { meteor create --package username:packagename } \n Replace "username" with your Meteor username & "packagename" with the name of your package. You can run this either inside your meteor application or in an external Package Folder.',
			header: '1. Create a package'
		},
		{
			_id: 2,
			className: 'list-group-item',
			text: 'In mac go to your ~/.bash_profile file, if you do not have this file create it and add a line, "export PACKAGE_DIRS="$HOME/path_to_your_package_folder". Restart your terminal. \n On windows, add a path to your Command Promt with path name = "PACKAGE_DIRS" and path = path_to_your_package_folder.',
			header: '2. If you created the package in an external folder, notify meteor where your package folder is.'
		},
		{
			_id: 3,
			className: 'list-group-item',
			text: 'You will need to configure your package.js file to provide important information to Meteor about your package.',
			header: '3. Set up your package.js file. Go into sample package\'s package.js for an example.'
		},
		{
			_id: 4,
			className: 'list-group-item',
			text: 'To use your package simple run the command - \n "meteor add package_name"',
			header: '4. Using your package'
		}];
		return(
			<div>
				<div className="row">
					<H1 className="text-center" text="This is a POC to show how Meteor Package works" />
				</div>
				<div className="container">		
					<hr />
					<div className="row">
						<div className="col-md-5 col-sm-12">
							<LeadP className="" text="All React Components in this POC App come from an internal meteor package" /> 
							<P className="" text="Internal because the UI Package being used here is not available on Atmospher.js. To get your package on atmosphere you will need to publish it."/>
							<LinkButton className="btn-lg primary" text="Sample Package Here" link="https://github.com/SureBro/bootstrap-component.git" />
						</div>
						<div className="col-md-5 col-md-offset-1 col-sm-12">
							<div className="row box light-grey">
								<H4 className="text-center" text="Steps to set-up a Meteor Package" />
							</div>
							<div className="row">
								<List className="list-group" items={steps} onItemClicked={null} />
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
})