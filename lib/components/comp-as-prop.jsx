/*
	This Component proves that you can send a component as a property
	to another component to render.
*/
CompAsProp = React.createClass({
	render(){
		var flexibleComponent = (<div className="box orange-bg"><H3 className="text-center" text="Example" /><P className="lead" text="This is a combination of 2 components that is being rendered indirectly by sending it as a property." /></div>)
		var steps = [{
			_id: 1,
			className: 'list-group-item',
			text: 'Example: var flexibleComponent = <P className="lead" text="This is a component that is being rendered indirectly by sending it as a property." />',
			header: '1. Create a variable in the render function before the return method.'
		},
		{
			_id: 2,
			className: 'list-group-item',
			text: 'Example: <FlexibleComponent component={flexibleComponent} />',
			header: '2. Send the component variable as property in your render method to a different component.'
		},
		{
			_id: 3,
			className: 'list-group-item',
			text: 'Example: return(<div>{this.props.component}</div>)',
			header: '3. In your component simply use the property as you would use any property'
		}];
		return(
			<div className="container">
				<div className="row">
					<H1 className="text-center" text="This is a POC to prove that React allows sending a component as a property"/>
				</div>
				<hr />
				<div className="row">
					<div className="col-md-5 col-sm-12">
						<FlexibleComponent component={flexibleComponent} />
					</div>
					<div className="col-md-5 col-md-offset-1 col-sm-12 instruction-block">
						<div className="row">
							<H3 className="text-center" text="Steps to replicate behavior" />
						</div>
						<div className="row">
							<List className="list-group" items={steps} onItemClicked={null} />
						</div>
					</div>
				</div>
				<hr />
			</div>
		)
	}
})