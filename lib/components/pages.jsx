/*
	This is the home page component
*/
HomePage = React.createClass({
	renderActions: function(){

	},
	render(){
		var compAsPropPath = FlowRouter.path('CompAsProp');
		var actionParentToChildrenPath = FlowRouter.path('ActionParentToChildren');
		var meteorConnectionPath = FlowRouter.path('meteorConnection');
		var meteorPackagePath = FlowRouter.path('meteorPath');
		
		return(
			<div>
				<div className="container">
					<div className="row">
						<H1 className="text-center" text="This is a POC application" />
					</div>
					<div className="row">
						<P className="text-center lead" text="Click on one of the buttons below to go to a particular POC." />
					</div>
					<div className="row">
						<div className="col-xs-6 col-md-3">
							<LinkButton className="btn-block" text="Comp As Prop" link={compAsPropPath} />
						</div>
						<div className="col-xs-6 col-md-3">
							<LinkButton className="btn-block" text="Action from Parent to Children" link={actionParentToChildrenPath} />
						</div>
						<div className="col-xs-6 col-md-3">
							<LinkButton className="btn-block" text="Meteor Connection" link={meteorConnectionPath} />
						</div>
						<div className="col-xs-6 col-md-3">
							<LinkButton className="btn-block" text="Meteor Package" link={meteorPackagePath} />
						</div>
					</div>
				</div>
				<hr />
				<div className="container">
					<H4 className="text-center" text="The sample code is on BitBucket. Click on the button to go to the repo." />
					<div className="text-center">
						<LinkButton className="btn-lg primary" text="Sample Code Here" link="https://bitbucket.org/2201pandey/9pipoc" />
					</div>
				</div>
			</div>
		)
	}
})