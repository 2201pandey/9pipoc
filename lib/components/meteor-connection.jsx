MeteorConnection = React.createClass({
	mixins:[ReactMeteorData],
	getMeteorData(){
		var data = [];
		data.isConnected = Meteor.status().connected;
		data.status = Meteor.status().status;

		return data;
	},
	checkConnectionStatus(e){
		var status = Meteor.status().status;

		alert('Connection status is ' + status);
	},
	render(){
		var alert = null;
		if(Meteor.isClient){
			console.log(this.data.status);
			if(! this.data.isConnected){
				var message = "";
				if(this.data.status === "connecting"){
					alert = <InfoAlert className="" message="Trying to connect to the server" />
				}else if(this.data.status === "failed"){
					alert = <DangerAlert className="" message="Connection has failed. There is no hope." />
				}else if(this.data.status === "waiting"){
					alert = <InfoAlert className="" message="Waiting for the right time to start connecting to the server" />
				}else if(this.data.status === "offline"){
					alert = <DangerAlert className="" message="Looks like you disconnected the internet. You mean" />
				}
			}
		}
		return(
			<div className="container">
				<div className="row">
					<H1 className="text-center" text="This is a POC to prove the usability of connection between client and server" />
				</div>
				<hr />
				<div className="row">
					<div className="col-md-5 col-sm-12">
						{alert}
						<div className="row">
							<H3 className="text-center" text="Try closing your internet connection. This does not work on localhost." />
							<P className="text-center" text="or" />
							<div className="text-center">
								<Button className="btn-info" text="Check connection status" onClick={this.checkConnectionStatus} />
							</div>
						</div>

					</div>
				</div>
				<hr />
			</div>
		)
	}
}) 