ActionFromParentToChildren = React.createClass({
	handleItemClicked(e){
		e.preventDefault();

		var value = e.target.dataset.value;
		console.log("Item clicked " + value);

		alert("You clicked on " + value);

		Meteor.call('logMessageOnServer', value);
	},
	render(){
		var items = [{
			_id: 1,
			className: 'list-group-item',
			text:'Child 1'
		},
		{	
			_id: 2,
			className: 'list-group-item',
			text:'Child 2'
		},
		{	
			_id: 3,
			className: 'list-group-item',
			text:'Child 3'
		},
		{	
			_id: 4,
			className: 'list-group-item',
			text:'Child 4'
		},
		{	
			_id: 5,
			className: 'list-group-item',
			text:'Child 5'
		}];
		var steps = [{
			_id: 1,
			className: 'list-group-item col-sm-12',
			text: 'Example: handleItemClicked(e){e.preventDefault();console.log("You clicked on the child component");},',
			header: '1. Create a function in your parent component'
		},
		{
			_id: 2,
			className: 'list-group-item col-sm-12',
			text: 'Example: <List className="list-group" items={items} onItemClicked={this.handleItemClicked} />',
			header: '2. Send the function as property to the child component'
		},
		{
			_id: 3,
			className: 'list-group-item col-sm-12',
			text: 'Example: <a className={this.props.className} onClick={this.props.handleClick} >{this.props.header}</a>',
			header: '3. In your child component set onClick on the component to the onClick property sent from parent component.'
		},
		{
			_id: 4,
			className: 'list-group-item col-sm-12',
			text: 'Example: In your child component ->  <a className={this.props.className} onClick={this.props.handleClick} data-value={this.props.dataAttribute}>{this.props.header}</a>. In your Parent Component onClick function -> var value = e.target.dataset.value;',
			header: '4. If you want to send data back to your parent component from your child component as param for your onClick function, use the "data-*" feature of HTML.'
		}];
		return(
			<div className="container">
				<div className="row">
					<H1 className="text-center" text="This is a POC to prove sending an action from parent to child." />
				</div>
				<hr />
				<div className="row">
					<div className="col-md-5 col-sm-12">
						<div className="row">
							<H3 className="text-center" text="Click on one of the Childs" />
						</div>
						<div className="row">
						<List className="list-group" items={items} onItemClicked={this.handleItemClicked} />
						</div>
					</div>
					<div className="col-md-5 col-md-offset-1 col-sm-12 instruction-block">
						<div className="row">
							<H3 className="text-center" text="Steps to replicate this behavior" />
						</div>
						<div className="row">
							<List className="list-group" items={steps} onItemClicked={null} />
						</div>
					</div>
				</div>
				<hr />
			</div>
		)
	}
})