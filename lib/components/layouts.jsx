/*
	This is the main layout used to render a page.
*/
MainLayout = React.createClass({
	render() {
		return (
			<div>
				<div  className='content-wrapper'>
					<main>
					</main>
					<body>
						{this.props.content}
					</body>
					<footer>
					</footer>
				</div>
			</div>
			);
	}
});
