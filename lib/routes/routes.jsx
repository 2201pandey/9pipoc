/*
	Goes to the home page
*/
FlowRouter.route('/', {
  	action() {
    	ReactLayout.render(MainLayout, {content: <HomePage />});
  	},
  	name: 'HomePage'
});
/*
	Goes to the Component as Props POC page
*/
FlowRouter.route('/compasprop', {
  	action() {
    	ReactLayout.render(MainLayout, {content: <CompAsProp />});
  	},
  	name: 'CompAsProp'
});
/*
	Goes to the Action from Parent to children as props POC page
*/
FlowRouter.route('/actionparenttochildren', {
  	action() {
    	ReactLayout.render(MainLayout, {content: <ActionFromParentToChildren />});
  	},
  	name: 'ActionParentToChildren'
});
/*
	Goes to the Meteor Connection POC page
*/
FlowRouter.route('/meteorconnection', {
  	action() {
    	ReactLayout.render(MainLayout, {content: <MeteorConnection />});
  	},
  	name: 'meteorConnection'
});
/*
	Goes to the Meteor Package POC page
*/
FlowRouter.route('/meteorpackage', {
  	action() {
    	ReactLayout.render(MainLayout, {content: <MeteorPackage />});
  	},
  	name: 'meteorPath'
});
/*
	This methods caches the view on the server. This is important because
	this project uses SSR(Server Side Rendering) and it is very CPU intensive
	to render a React component. We need SSR for SEO.
*/
if(Meteor.isServer) {
  var timeInMillis = 1000 * 10; // 10 secs
  FlowRouter.setPageCacheTimeout(timeInMillis)
}