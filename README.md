# README #

### What is this repository for? ###

This is a meteor application that contains all POCs for Meteor App. Each POC page has an example and a step by step guide on how to get things to work. You can either use the guide or clone this repo and view the sample code directly.

### How do I get set up? ###

* Clone the repo
* If you don't have Meteor setup, set up meteor
* In the app directory run meteor command.
* At the home page click on the POC you want to view.

### Contribution guidelines ###
* If you add a new POC, make sure you add a link to the home page so people can view the POC.
* Make sure to have a guide and an example on your POC page.